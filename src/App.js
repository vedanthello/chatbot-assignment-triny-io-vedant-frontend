import React, { useState, useEffect } from "react";
import axios from "axios";
import {
	Card,
	Alert,
  ListGroup
} from 'react-bootstrap';

export default function App() {
  const [message, updateMessage] = useState("Fetching them. Just a moment...");
  const [error, setError] = useState("");
  const [list, updateList] = useState([]);

  useEffect(() => {
    
    const url = window.location.host.includes("localhost")
      ? "http://localhost:5000"
      : "https://chatbot-triny-io-vedant.herokuapp.com/"
    ;
    
    axios.get(url)
    .then(res => {
      setError("");
      updateMessage("");
      updateList(res.data);
    })
    .catch(err => {
      setError("Failed to fetch!");
      updateMessage("");
    });

  }, []);

  return (
    <div className="background">
      <Card className="foreground" border="primary">

        <Card.Header className="foreground-header">List of intents</Card.Header>

        <Card.Body className="list-container">
          
          {message && 
            <Alert variant={"secondary"}>
              {message}
            </Alert>
          }

          {error &&
            <Alert variant={"danger"}>
              {error}
            </Alert>
          }

          {list && (
            <ListGroup variant="flush">
              {list.map((intent, index) => (
                <ListGroup.Item key={index}>{intent.displayName}</ListGroup.Item>
              ))}
            </ListGroup>
          )}

        </Card.Body>

      </Card>
    </div>
  );
}